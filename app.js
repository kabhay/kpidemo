﻿/*-----------------------------------------------------------------------------
LUIS Chatbot 
-----------------------------------------------------------------------------*/

var restify = require('restify');
var builder = require('botbuilder');
var botbuilder_azure = require("botbuilder-azure");
var request = require("request");

/***************** Include app functions *************** */ 
var fs = require('fs');
eval(fs.readFileSync('app_functions.js').toString());            // imported app_functions.js
//eval(fs.readFileSync('app_top_brand_functions.js').toString());  // imported app_top_brand_functions.js


var global_user_login_failed_message = 'Sorry!! You are not authorized to chat with me. Please contact admin';
var global_hi_welcome_msg = 'Welcome User, I am all set to help you with your finance queries.';
var global_welcome_message = "Hi there, please give me a moment to authenticate you...\n\n";

var global_did_not_understand_msg = "Sorry!, I don't understand, please ask me questions about your finance KPIs";

var global_not_found_in_db = "Sorry! Either there is no data for this combination or you may not have access to this data. Please retry";

var global_ask_month_year = "Please provide month and year (Example: Mar 2017)";

// var global_database_table_name = 'rb_chat_bt_tbl_new4';
// var global_database_table_name = 'v_rb_sales_data';
// var global_database_table_name = 'v_rb_sales_data_1';
// var global_database_table_name = 'v_rb_sales_data_2';
var global_database_table_name = 'kpidb';

var global_access_dataset_name = 'access_list';
// var global_access_dataset_name = 'access_list_brand_test';

var global_location_not_found = "Oops!! I am not aware of this country. Please check spelling";
var global_month_not_found = "Oops!! I am not aware of this month and year. Please check spelling";

// var global_query_currency_header=""+
// "select "+ 
// "case "+ 
// "when (FLOOR(LOG10(ABS(sum(ytd)))) + 1) > 9 then concat('£', cast(round(sum(ytd)/1000000000,2) as numeric(36,2)),' Billion') "+
// "when (FLOOR(LOG10(ABS(sum(ytd)))) + 1) > 6 and (FLOOR(LOG10(ABS(sum(ytd)))) + 1) <= 9 then concat('£', cast(round(sum(ytd)/1000000,2) as numeric(36,2)),' Million') "+
// "when (FLOOR(LOG10(ABS(sum(ytd)))) + 1) > 3 and (FLOOR(LOG10(ABS(sum(ytd)))) + 1) <= 6 then concat('£', cast(round(sum(ytd)/1000,2) as numeric(36,2)),' K') "+
// "else FORMAT(sum(ytd),'£#,0') "+
// "end as sumofamount from "+
// "";

var global_query_currency_header=""+
"select "+ 
"case "+ 
"when (len(ABS(sum(ytd))) - 10) > 9 then concat('£', cast(round(sum(ytd)/1000000000,2) as numeric(36,2)),' Billion') "+
"when (len(ABS(sum(ytd))) - 10) > 6 and (len(ABS(sum(ytd))) - 10) <= 9 then concat('£', cast(round(sum(ytd)/1000000,2) as numeric(36,2)),' Million') "+
"when (len(ABS(sum(ytd))) - 10) > 3 and (len(ABS(sum(ytd))) - 10) <= 6 then concat('£', cast(round(sum(ytd)/1000,2) as numeric(36,2)),' K') "+
"else FORMAT(sum(ytd),'£#,0') end as sumofamount from "+
"";

// var global_query_currency_header_monthly=""+
// "select "+ 
// "case "+ 
// "when (FLOOR(LOG10(ABS(sum(mo)))) + 1) > 9 then concat('£', cast(round(sum(mo)/1000000000,2) as numeric(36,2)),' Billion') "+
// "when (FLOOR(LOG10(ABS(sum(mo)))) + 1) > 6 and (FLOOR(LOG10(ABS(sum(mo)))) + 1) <= 9 then concat('£', cast(round(sum(mo)/1000000,2) as numeric(36,2)),' Million') "+
// "when (FLOOR(LOG10(ABS(sum(mo)))) + 1) > 3 and (FLOOR(LOG10(ABS(sum(mo)))) + 1) <= 6 then concat('£', cast(round(sum(mo)/1000,2) as numeric(36,2)),' K') "+
// "else FORMAT(sum(mo),'£#,0') "+
// "end as sumofamount from "+
// "";


var global_query_currency_header_monthly=""+
"select "+ 
"case "+ 
"when (len(ABS(sum(mo))) - 10) > 9 then concat('£', cast(round(sum(mo)/1000000000,2) as numeric(36,2)),' Billion') "+
"when (len(ABS(sum(mo))) - 10) > 6 and (len(ABS(sum(mo))) - 10) <= 9 then concat('£', cast(round(sum(mo)/1000000,2) as numeric(36,2)),' Million') "+
"when (len(ABS(sum(mo))) - 10) > 3 and (len(ABS(sum(mo))) - 10) <= 6 then concat('£', cast(round(sum(mo)/1000,2) as numeric(36,2)),' K') "+
"else FORMAT(sum(mo),'£#,0') end as sumofamount from "+
"";


// Setup Restify Server
var server = restify.createServer();
server.listen(process.env.port || process.env.PORT || 3978, function () {
   console.log('%s listening to %s', server.name, server.url); 
});
  
// Create chat connector for communicating with the Bot Framework Service
var connector = new builder.ChatConnector({
    appId: process.env.MicrosoftAppId,
    appPassword: process.env.MicrosoftAppPassword,
    openIdMetadata: process.env.BotOpenIdMetadata 
});

// Listen for messages from users 
server.post('/api/messages', connector.listen());

/*----------------------------------------------------------------------------------------
* Bot Storage: This is a great spot to register the private state storage for your bot. 
* We provide adapters for Azure Table, CosmosDb, SQL Azure, or you can implement your own!
* For samples and documentation, see: https://github.com/Microsoft/BotBuilder-Azure
* ---------------------------------------------------------------------------------------- */

//var tableName = 'botdata';
//var azureTableClient = new botbuilder_azure.AzureTableClient(tableName, process.env['AzureWebJobsStorage']);
//var tableStorage = new botbuilder_azure.AzureBotStorage({ gzipData: false }, azureTableClient);
var inMemoryStorage = new builder.MemoryBotStorage();

// Create your bot with a function to receive messages from the user
var bot = new builder.UniversalBot(connector, function (session, args) {
    // 'I do not know' - Event Handler block    
    loginIds(session.message.address.user.id).then(result => {
        if (result == true) {
            session.say("Sorry!, I don't understand, please ask me questions about your finance KPIs","Sorry!, I don't understand, please ask me questions about your finance KPIs");    
        }
        else {
            session.send(global_user_login_failed_message);    
        }   
    });               
});

//bot.set('storage', tableStorage);
bot.set('storage', inMemoryStorage);

// Make sure you add code to validate these fields
var luisAppId = '5c326a65-239f-40ed-b95b-f093b5f01cdb';
var luisAPIKey = '2eba3497040048af8d2651c2882521fe';
var luisAPIHostName = process.env.LuisAPIHostName || 'westus.api.cognitive.microsoft.com';
var bingAPIKey = 'aa3f09478d47402c93d9aef3519c70ef';

//const LuisModelUrl = 'https://' + luisAPIHostName + '/luis/v1/application?id=' + luisAppId + '&subscription-key=' + luisAPIKey;
const LuisModelUrl = 'https://' + luisAPIHostName + '/luis/v2.0/apps/' + luisAppId + '?subscription-key=' + luisAPIKey;
console.log(LuisModelUrl);
var global_luis_bing_rest_api = "https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/"+luisAppId+"?subscription-key="+luisAPIKey+"&spellCheck=true&bing-spell-check-subscription-key="+bingAPIKey+"&verbose=false&q=";

// Main dialog with LUIS
var recognizer = new builder.LuisRecognizer(LuisModelUrl);
bot.recognizer(recognizer);
var intents = new builder.IntentDialog({ recognizers: [recognizer] })
console.log('Recognized intent is: ');
console.log(intents.intent);
console.log('\n\n');


/*
Dialog to Ask for the Month and Year (Nested follow-up) - No Intents
*/
bot.dialog('FeedbackDialog', [
    function (session, args, next) {
            builder.Prompts.text(session, 'You are welcome ! Please provide me your feedback now');
    },
    function (session, results) {
        //session.endDialog(`The location specified is ${results.response}!`);
        var user_response = results.response;
        // var user_response_lowercase = user_response.toLowerCase();
        
        /*** STORE INTO FEEDBACK HISTORY  ***/
        email_id = session.message.address.user.id;
        //store_feedback_history(email_id, user_response);
        session.send('Thank you for sharing your feedback with me');
        
    }  // Closing of the 2nd function
]);


/*
Dialog to Ask for the Location - No Intents
*/
bot.dialog('AskLocationDialog', [
    function (session, args, next) {
            builder.Prompts.text(session, "Please provide country name");
    },
    function (session, results) {
        //session.endDialog(`The location specified is ${results.response}!`);
        var user_response = results.response;
        var prefix_string = "Gross Margin for ";
        
        user_response = prefix_string + user_response;
        
        user_response = user_response.replace(" ","%20");  // Replace whitespace with its ASCII character
        
        query_string = global_luis_bing_rest_api + user_response;
        
        request(query_string, function (error, response, body) {
                console.log('error:', error); 
                console.log('statusCode:', response && response.statusCode);
                // console.log(body);
                var result_json = JSON.parse(body);
                console.log(result_json.entities);
                
                var location = builder.EntityRecognizer.findEntity(result_json.entities, 'Location');
                
                if ((location)) {
                    /* Sql variables */
                    if (location.resolution.values[0] != "") {             // If Location values array in LUIS JSON is not empty
                        session.userData.global_location = "" + location.resolution.values[0]; // Get the 1st element from LUIS JSON values array, and convert to String
                    }
                    else {
                        session.userData.global_location = location.entity;
                    }
                    
                    session.userData.global_location_user_friendly = nodeUcfirst(session.userData.global_location); // First character - uppercase
                    
                    session.userData.global_location = session.userData.global_location.toUpperCase();             // All characters to uppercase for DB
                    
                    session.userData.global_location = "'"+session.userData.global_location+"'";  
                    
                    if ((session.userData.global_month_user_friendly == 'Mar') && (session.userData.global_year_user_friendly == '2018')) {
                        global_query(session, 'MTD', 'for', session.userData.global_monthly_keyword_found);   
                    }
                    else {
                        global_query(session, 'YTD', 'upto', session.userData.global_monthly_keyword_found);       
                    } 
                    
				} // Closing of if -  Location
                
                else {  // Else - Location not specified                       
                        session.send(global_location_not_found);   
                        session.beginDialog('AskLocationDialog');   
                }

        });  // Closing of the REST API - send receive block
                       
        session.endDialog();
    }  // Closing of the 2nd function
]);



/*
Dialog to Ask for the Location Nested - No Intents
*/
bot.dialog('AskLocationNestedDialog', [
    function (session, args, next) {
            builder.Prompts.text(session, "Please provide Country Name");
    },
    function (session, results) {
        var user_response = results.response;
        var prefix_string = "Gross Margin for ";
        
        user_response = prefix_string + user_response;
        
        user_response = user_response.replace(" ","%20");  // Replace whitespace with its ASCII character
        
        query_string = global_luis_bing_rest_api + user_response;
        
        request(query_string, function (error, response, body) {
                console.log('error:', error); 
                console.log('statusCode:', response && response.statusCode);
                var result_json = JSON.parse(body);
                console.log(result_json.entities);
                
                var location = builder.EntityRecognizer.findEntity(result_json.entities, 'Location');
                
                if ((location)) {
                    /* Sql variables */
                    if (location.resolution.values[0] != "") {             // If Location values array in LUIS JSON is not empty
                        session.userData.global_location = "" + location.resolution.values[0]; // Get the 1st element from LUIS JSON values array, and convert to String
                    }
                    else {
                        session.userData.global_location = location.entity;
                    }
                    
                    session.userData.global_location_user_friendly = nodeUcfirst(session.userData.global_location); // First character - uppercase
                    
                    session.userData.global_location = session.userData.global_location.toUpperCase();             // All characters to uppercase for DB
                    
                    session.userData.global_location = "'"+session.userData.global_location+"'";  
                    
                    session.beginDialog('AskMonthYearNestedDialog');  // Ask the month and year  - Nested follow-up Dialog
                    
                    
				} // Closing of if -  location
                
                else {  // Else - Location not specified
                         //session.say(`${global_location_not_found}`,`${global_location_not_found}`);
                         session.send(global_location_not_found);
                         session.beginDialog('AskLocationNestedDialog');                     
                }
        });  // Closing of the REST API - send receive block
                       
        session.endDialog();
    }  // Closing of the 2nd function
]);


/*
Dialog to Ask for the Month and Year (Nested follow-up) - No Intents
*/
bot.dialog('AskMonthYearNestedDialog', [
    function (session, args, next) {
            builder.Prompts.text(session, global_ask_month_year);
    },
    function (session, results) {
        var user_response = results.response;
        var user_response_lowercase = user_response.toLowerCase();
        session.userData.global_day = "";
                            
        boilerPlateCodeMonthYear(session, user_response, 'AskMonthYearNestedDialog');
    }  // Closing of the 2nd function
]);


/*
Dialog to Ask for the Month and Year - No Intents
*/
bot.dialog('AskMonthYearDialog', [
    function (session, args, next) {
            builder.Prompts.text(session, global_ask_month_year);
    },
    function (session, results) {
        var user_response = results.response;
        var user_response_lowercase = user_response.toLowerCase();
        
        session.userData.global_day = '';       
        boilerPlateCodeMonthYear(session, user_response, 'AskMonthYearDialog'); //function imported from app_functions.js
    }  // Closing of the 2nd function
]);



/*
Dialog to get Gross Margin Information - Triggered by 'Margin' Intent
*/
bot.dialog('MarginDialog',
    (session, args) => {       
        loginIds(session.message.address.user.id).then(result => {
            console.log("Matched value is: "+ result); 
            if (result == true) {
                var kpi = 'Gross Margin 3rd Party';
                var kpi_user_friendly = 'Gross Margin';
                var intent = args.intent;
                                           
                commonCodeIntents(session, intent, kpi, kpi_user_friendly); 
                boilerPlateCodeIntents(session, intent);
                    
            }
            else {
                session.send(global_user_login_failed_message);
            }
        });           
}).triggerAction({
    matches: 'Margin'
})


/*
Dialog to get Gross Sales Information - Triggered by 'Sales' Intent
*/
bot.dialog('SalesDialog',
    (session, args) => {       
        loginIds(session.message.address.user.id).then(result => {
            console.log("Matched value is: "+ result); 
            if (result == true) {
                var kpi = 'Gross Sales 3rd Party';
                var kpi_user_friendly = 'Gross Sales';
                var intent = args.intent;
                
                commonCodeIntents(session, intent, kpi, kpi_user_friendly);                                 
                boilerPlateCodeIntents(session, intent);                    
            }
            else {
                session.send(global_user_login_failed_message);
            }
        });           
}).triggerAction({
    matches: 'Sales'
})



/*
Dialog to get COP Information - Triggered by 'COP' Intent
*/
bot.dialog('COPDialog',
    (session, args) => {       
        loginIds(session.message.address.user.id).then(result => {
            console.log("Matched value is: "+ result); 
            if (result == true) {
                var kpi = 'COP';
                var kpi_user_friendly = 'COP';
                var intent = args.intent;
                
                commonCodeIntents(session, intent, kpi, kpi_user_friendly);                                        
                boilerPlateCodeIntents(session, intent);                    
            }
            else {
                session.send(global_user_login_failed_message);
            }
        });           
}).triggerAction({
    matches: 'COP'
})


/*
Dialog to get Net Revenue Information - Triggered by 'Revenue' Intent
*/
bot.dialog('RevenueDialog',
    (session, args) => {       
        loginIds(session.message.address.user.id).then(result => {
            console.log("Matched value is: "+ result); 
            if (result == true) {
                var kpi = 'Net Revenue Third Party';
                var kpi_user_friendly = 'Revenue';
                                
                var intent = args.intent;
                
                commonCodeIntents(session, intent, kpi, kpi_user_friendly);                          
                
                session.userData.global_top = '';
                session.userData.global_bottom = '';    

                // /*** Top / Bottom Brands - based on revenue  ***/
                var top = builder.EntityRecognizer.findEntity(intent.entities, 'Top');
    
                if (top) {
                    if (top.resolution.values[0] != "") {             // If 'Top' values array in LUIS JSON is not empty
                        session.userData.global_top = "" + top.resolution.values[0]; // Get the 1st element from LUIS JSON values array, and convert to String
                    }
                    else {
                        session.userData.global_top = top.entity;
                    }
                }
                    
               var bottom = builder.EntityRecognizer.findEntity(intent.entities, 'Bottom');
    
                if (bottom) {                    
                    if (bottom.resolution.values[0] != "") {             // If 'Bottom' values array in LUIS JSON is not empty
                        session.userData.global_bottom = "" + bottom.resolution.values[0]; // Get the 1st element from LUIS JSON values array, and convert to String
                    }
                    else {
                        session.userData.global_bottom = bottom.entity;
                    }   
                }
                
                console.log(top);
                console.log(bottom);
                
                if ((top) || (bottom)) {
                    console.log('Entered top bottom handler block');
                    topBottomBrandsHandler(session, intent, session.userData.global_top, session.userData.global_bottom);    
                }
                else {
                    boilerPlateCodeIntents(session, intent);    
                }   
            }
            else {
                session.send(global_user_login_failed_message);
            }
        });
           
}).triggerAction({
    matches: 'Revenue'
})



/*
Dialog to get Greeting Information - Triggered by 'Greeting' Intent
*/
bot.dialog('GreetingDialog',
    (session, args) => {
        console.log('The user accessing is: '); 
        console.log(session.message.user.name);
        console.log(session.message.address.user.id);
        user_id = session.message.address.user.id;
        
        console.log('Greeting Intent is triggered');           
         loginIds(session.message.address.user.id).then(result => {
            console.log("Matched value is: "+ result); 
            if (result == true) {                
                try {
                    var intent = args.intent;  
                    getUsernameFromDB(user_id).then(result2 => {                // Get the username from DB using function call
                        console.log('DB user return value: '+result2);
                        if (result2 != null) {
                            username = result2;   
                        }
                        else {
                            username = 'User';  
                        } 
                        
                        session.send('Hi '+ username +', I am here to answer your FPI related queries.');
                        session.send('Please ask your query else type HELP to know more');                                     
                    });
                } catch (error) {
                    session.send(global_did_not_understand_msg);   
                }
            }  
            else {
                session.send('Sorry!! You are not authorized to chat with me. Please contact admin');
            }
        });        
}
).triggerAction({
    matches: 'Greeting'
})


/*Dialog to get Introduction Information - Triggered by 'Introduction' Intent
*/
bot.dialog('IntroductionDialog',
    (session, args) => {
        loginIds(session.message.address.user.id).then(result => {
            console.log("Matched value is: "+ result); 
            if (result == true) {
                try {
                    var intent = args.intent;
                    session.send('I am programmed to help you provide data of your FPI. Some common queries are');
                    session.send('• What was YTD revenue of RB upto Dec 2017');
                    session.send('• Show me MO sales of durex in UK in last month');
                    session.send('• What is YTD margin for nurofen in Germany');
                                                                      
                    getLastDayMonthYearFromDB().then(result_db_lastdate => {  // Retrieve the Last Date from Database
                        console.log("Matched value is: "+ result_db_lastdate); 
                        if (result_db_lastdate != null) {                 
                            session.send('Period Covered: Jan 2017 to '+ result_db_lastdate +'');
                            session.send('FPI Covered: Revenue, Sales, Margin & Profit');
                            session.send('Please specify YTD or MO in your query to get YTD and monthly data');
                        }
                        else {
                            session.send(global_hi_welcome_msg);   
                        }
                    });
                                                
                } catch (error) {
                    session.send(global_did_not_understand_msg);   
                } 
            }
            else
                session.send(global_user_login_failed_message); 
        });
          
}
).triggerAction({
    matches: 'Introduction'
})

/*Dialog to get Thanks Information - Triggered by 'Thanks' Intent
*/
bot.dialog('ThanksDialog',
    (session, args) => {
        loginIds(session.message.address.user.id).then(result => {
            console.log("Matched value is: "+ result); 
            if (result == true) {
                try {
                    var intent = args.intent;
                    session.beginDialog('FeedbackDialog');
                } catch (error) {
                    session.send(global_did_not_understand_msg);   
                } 
            }
            else
                session.send(global_user_login_failed_message);   
        });          
}
).triggerAction({
    matches: 'Thanks'
})


/*
Dialog to Exit conversation - Triggered by 'Exit' Intent
*/
bot.dialog('ExitDialog',
    (session, args) => {
        loginIds(session.message.address.user.id).then(result => {
            console.log("Matched value is: "+ result); 
            if (result == true) {
                try {
                    var intent = args.intent;
                    session.endConversation('It was a pleasure helping you. Bye !');
                    clearGlobalVariablesIntents(session);
                    
                } catch (error) {
                    session.send(global_did_not_understand_msg);   
                } 
            }
            else {
                session.send(global_user_login_failed_message);   
                clearGlobalVariablesIntents(session);
            }
        });          
}
).triggerAction({
    matches: 'Exit'
})



/*
Hello Message at the beginning of the conversation
*/
bot.on('conversationUpdate', function(message) {
    // Send a hello message when bot is added
    if (message.membersAdded) {
        message.membersAdded.forEach(function(identity) {
            if (identity.id === message.address.bot.id) {
                // global_user_authentication = false;
                var reply = new builder.Message().address(message.address).text(global_welcome_message);
                bot.send(reply);
            }
        });
    }
});



console.log("test");
var Sequelize = require('sequelize');
var sequelize = new Sequelize('aicoach4demo', 'myadmin@aicoach', 'Infy@1234', {
  host: 'aicoach.database.windows.net',
  dialect: 'mssql', 
  dialectOptions: {
    encrypt: true
  }
}); 

