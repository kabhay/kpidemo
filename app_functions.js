/******* App Functions  *******/

function getCurrentMonth() {
    var m_names = ['Jan', 'Feb', 'Mar', 
                   'Apr', 'May', 'Jun', 'Jul', 
                   'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
                     
    d = new Date();
    var n = m_names[d.getMonth()];  
    var str_month = n.toString();     
    return str_month;      
}

function getPreviousMonth() {
    var m_names = ['Jan', 'Feb', 'Mar', 
                   'Apr', 'May', 'Jun', 'Jul', 
                   'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    
    d = new Date();
    
    var this_month_index = d.getMonth();
    var previous_month_index = this_month_index - 1;
    
    var n = m_names[previous_month_index];  
    var str_month = n.toString();     
    return str_month;      
}

function getCurrentYear() {
    d = new Date();
    var year = d.getFullYear();
    var str_year = year.toString();
    return str_year;        
}

function getLastDayInDB() {
      
    var db_last_day = null;   
    return new Promise((resolve, reject) => {
    sequelize.query(""+
    "select last_date as upto_last_date from ("+
    "select day(DATEADD(mm, 0, max(date_updated2))) as last_date "+
    "from "+global_database_table_name+" "+
    "where year = "+getCurrentYear()+" ) a",{type: sequelize.QueryTypes.SELECT
        })
        .then(result_last_day => {
              console.log(result_last_day);
            
              if (result_last_day.length != 0) {
                    if (result_last_day[0].upto_last_date != null) {
                        db_last_day = result_last_day[0].upto_last_date;
                        console.log('DB last day is: ' + result_last_day[0].upto_last_date);
                    }    
                    else {
                        db_last_day = null; 
                    }
              } else {
                db_last_day = null;
              }
              resolve(db_last_day);
        })
    })        
            
}

function getLastMonthInDB() {
    var db_last_month = null;   
    return new Promise((resolve, reject) => {
        sequelize.query("select CAST(DATENAME(month,DATEADD(mm,0,max(date_updated2))) AS CHAR(3)) as max_month "+
        "from " + global_database_table_name + "",{type: sequelize.QueryTypes.SELECT
        })
        .then(result_last_month => {
              console.log(result_last_month);
            
              if (result_last_month.length != 0) {
                    if (result_last_month[0].max_month != null) {
                        db_last_month = result_last_month[0].max_month;
                        console.log('DB last month is: ' + result_last_month[0].max_month);
                    }    
                    else {
                        db_last_month = null; 
                    }
              } else {
                db_last_month = null;
              }
              resolve(db_last_month);
        })
    })        
}


function getLastDayMonthYearFromDB() {
    var db_last_date = null;
    
    return new Promise((resolve, reject) => {
        sequelize.query("select CAST(DATENAME(day,DATEADD(mm,0,max(date_updated2))) AS CHAR(2)) + ' ' +"+
            "CAST(DATENAME(month,DATEADD(mm,0,max(date_updated2))) AS CHAR(3)) + ' ' +"+
            "CAST(DATENAME(year,DATEADD(mm,0,max(date_updated2))) AS CHAR(4)) as max_date "+ 
            "from "+global_database_table_name+"",{type: sequelize.QueryTypes.SELECT
        })
        .then(result => {
              console.log(result);
            
              if (result.length != 0) {
                    if (result[0].max_date != null) {
                        db_last_date = result[0].max_date;
                        console.log('DB last date is: ' + result[0].max_date);
                    }    
                    else {
                        db_last_date = null; 
                    }
              } else {
                db_last_date = null;
              }
              resolve(db_last_date);
        })
    })
}



function nodeUcfirst(str) // Uppercase the first character of month
{
    return str.charAt(0).toUpperCase() + str.slice(1);
}

// Get the month name in a 3 letter format
function getThreeLetterMonthNames(month_name) {
    month_name = month_name.toLowerCase();
    
    if (month_name == 'january' || month_name == 'jan') 
        month_name = 'jan';
    else if (month_name == 'february' || month_name == 'feb') 
        month_name = 'feb';
    else if (month_name == 'march' || month_name == 'mar') 
        month_name = 'mar';
    else if (month_name == 'april' || month_name == 'apr') 
        month_name = 'apr';
    else if (month_name == 'may' || month_name == 'may') 
        month_name = 'may';
    else if (month_name == 'june' || month_name == 'jun') 
        month_name = 'jun';   
    else if (month_name == 'july' || month_name == 'jul') 
        month_name = 'jul';               
    else if (month_name == 'august' || month_name == 'aug') 
        month_name = 'aug';  
    else if (month_name == 'september' || month_name == 'sep') 
        month_name = 'sep';  
    else if (month_name == 'october' || month_name == 'oct') 
        month_name = 'oct';  
     else if (month_name == 'november' || month_name == 'nov') 
        month_name = 'nov';  
    else  if (month_name == 'december' || month_name == 'dec')    // put else if in the logic
        month_name = 'dec';
    else
        month_name = '';
        
    return nodeUcfirst(month_name);                   
}

function loginIds(email_id) {
    var is_matched = false;
    
    return new Promise((resolve, reject) => {
        sequelize.query("SELECT DISTINCT emailid FROM "+global_access_dataset_name+" where emailid='" + email_id + "'", {
          type: sequelize.QueryTypes.SELECT
        })
        .then(result => {
              console.log(result);
              // console.log('DB email id is: ' + result[0].emailid);
            
              if (result.length != 0) {
                console.log('True block');
                is_matched = true;
              } else {
                is_matched = true;
              }
              resolve(is_matched);
        })
    })
}


// Check whether yesterday, today, etc. exist in the user input
function checkExists(new_str) {
  is_matched = false;
  var arr_str = ['yesterday', 'today', 'day before', 'day before yesterday'];
  
  for (i = 0; i < arr_str.length; i++) { 
  	if (arr_str[i] == new_str) {
    		is_matched = true;
    		return is_matched;
            break;
    }
  }
  return is_matched;
}

// Check whether Brand exists in the list - to filter
function brandFilterExists(new_str) {
  is_matched = false;
  var arr_str = ['RB'];
  
  for (i = 0; i < arr_str.length; i++) { 
  	if (arr_str[i] == new_str) {
    		is_matched = true;
    		return is_matched;
            break;
    }
  }
  return is_matched;
}


function getUsernameFromDB(email_id) {
    var db_username = null;
    
    return new Promise((resolve, reject) => {
        sequelize.query("select distinct SUBSTRING(username , 1, CHARINDEX(' ', username) - 1) as username FROM "+ global_access_dataset_name +" where emailid='" + email_id + "'", {
          type: sequelize.QueryTypes.SELECT
        })
        .then(result => {
              console.log(result);
            
              if (result.length != 0) {
                    if (result[0].username != null) {
                        db_username = result[0].username;
                        console.log('DB email id is: ' + result[0].username);
                    }    
                    else {
                        db_username = null; 
                    }
              } else {
                db_username = null;
              }
              resolve(db_username);
        })
    })
}



function store_chat_history(email_id, intent_name, user_message) {
       
}


function store_feedback_history(email_id, user_message) {
   
}


function global_query(session, duration, for_or_upto, monthly_keyword_found) {
    try{
            session.userData.global_monthly_keyword_found = monthly_keyword_found;
            
            // Concatenate the month and year for comparison
            session.userData.merged_month_year = session.userData.global_month_user_friendly + session.userData.global_year_user_friendly;
            
            /* Complete the sentence for empty brand and empty location */
        
            if (session.userData.global_brand_user_friendly == '') {
                session.userData.global_brand_user_friendly = 'RB';
            }
            
            if (session.userData.global_location_user_friendly == '') {
                session.userData.global_location_user_friendly = 'total';
            }
            
            var daily_limit_string = '';
            
            if (duration == 'MTD' && for_or_upto == 'for') {
                try {
                    sequelize.query(""+
                    "select concat('Upto ',last_date) as upto_last_date from ("+
                    "select day(DATEADD(mm, 0, max(date_updated2))) as last_date "+
                    "from "+global_database_table_name+" "+
                    "where year = 2018 ) a", { type: sequelize.QueryTypes.SELECT})
                      .then(result => {
                        console.log(result);
                        if ((result[0].upto_last_date) != null) {          // When the End Result is not null
                            console.log(result[0].upto_last_date);                                  
                             daily_limit_string = "("+result[0].upto_last_date+" "+session.userData.global_last_month_in_db+") ";     
                             // 'Mar' is the last month in DB                                                                     
                        }
                        else {
                             daily_limit_string = '';    
                        }
                      })
                }
                catch (error) {
                    daily_limit_string = '';
                } 
            }
        
            console.log('Daily limit String: '+daily_limit_string);
    
            if (session.userData.global_monthly_keyword_found == false && session.userData.merged_month_year != 'Apr2018') {     // Non-Monthly query
                sequelize_piece_global_query(session, duration, for_or_upto, daily_limit_string, global_query_currency_header);        
            }            
            else {                                                  // Monthly query
                // Change duration to 'MO' for 'YTD'
                duration = 'MO';
                for_or_upto = 'for';
                sequelize_piece_global_query(session, duration, for_or_upto, daily_limit_string, global_query_currency_header_monthly);                   
            }   // Closing of else - monthly
                                                  
    } catch (error) {
            session.say(`I am sorry. I do not know the data`,`I am sorry. I do not know the data`);
    }        
}

function sequelize_piece_global_query(session, duration, for_or_upto, daily_limit_string, query_header) {
    sequelize.query(""+
    query_header+
    // "select SUM(mo) sumofamount from "+
    global_database_table_name+" "+ 
    "where country in (select country from "+ global_access_dataset_name +" where emailid = '"+session.message.address.user.id+"')"+
    // "And brand in (select brand from "+ global_access_dataset_name +" where emailid = '"+session.message.address.user.id+"') " +
    " OR fpi = ISNULL(fpi,"+(session.userData.global_kpi|| null)+") "+
    "OR scenario_code = ISNULL(scenario_code,'ACT') "+                            
    "OR brand = ISNULL(brand,"+(session.userData.global_brand|| null)+") "+
    "OR country = ISNULL(country,"+(session.userData.global_location|| null)+") "+
    "OR month = ISNULL(month,"+(session.userData.global_month|| null)+") "+
    "OR year = ISNULL(year,"+(session.userData.global_year|| null)+") ", { type: sequelize.QueryTypes.SELECT})
      .then(result => {
        console.log(result);
        if (result.length != 0) {
        // if ((result[0].sumofamount) != null) {          // When the End Result is not null                                  
            if ((result[0].sumofamount) != null) {                                    
                session.say(`${duration} ${session.userData.global_kpi_user_friendly} of ${session.userData.global_brand_user_friendly} ${for_or_upto} ${session.userData.global_month_user_friendly} ${session.userData.global_year_user_friendly} ${daily_limit_string}in ${session.userData.global_location_user_friendly} is ${result[0].sumofamount}`,`${duration} ${session.userData.global_kpi_user_friendly} of ${session.userData.global_brand_user_friendly} ${for_or_upto} ${session.userData.global_month_user_friendly} ${session.userData.global_year_user_friendly} ${daily_limit_string}in ${session.userData.global_location_user_friendly} is ${result[0].sumofamount}`);
            }
            else {
                session.send(global_not_found_in_db);     
            }                                                                       
        }
        else {
            session.send(global_not_found_in_db);    
        }
      })         
}


// Query for YTD data (daily)
function global_query_daily(session, from_date, to_date, final_day_in_database, final_month_in_database, monthly_keyword_found) {


    // Concatenate the month and year for comparison
    session.userData.merged_month_year = session.userData.global_month_user_friendly + session.userData.global_year_user_friendly;
    console.log('\n\nMerged month year: '+session.userData.merged_month_year);                   
                       
    // var final_month_in_database = getCurrentMonth();
    var final_year_in_database = '2018';
    
    session.userData.global_monthly_keyword_found = monthly_keyword_found;
    // var final_day_in_database = last_day_in_db;
    
    console.log('Global Query Daily');
    console.log(session.userData.global_month);
    console.log(session.userData.global_year);
    console.log('Final month in database is: '+final_month_in_database); 
    
    try{
 
            getLastDayInDB().then(result_db_lastday => {  // Retrieve the Last Day from Database
                if (result_db_lastday != null) {  
                    final_day_in_database =  result_db_lastday; 
                }
                else {
                    final_day_in_database = '2';    
                }
            });    
            
            /* Complete the sentence for empty brand and empty location */
            
            if (session.userData.global_brand_user_friendly == '') {
                session.userData.global_brand_user_friendly = 'RB';
            }
            
            if (session.userData.global_location_user_friendly == '') {
                session.userData.global_location_user_friendly = 'total';
            }
            
            from_date_user_friendly = from_date;
            from_date = "'"+from_date+"'";
            
            to_date_user_friendly = to_date;
            to_date = "'"+to_date+"'"; 
        
            if (session.userData.global_monthly_keyword_found == false && session.userData.merged_month_year != 'Apr2018') {                // Non-monthly query
                sequelize.query(""+
                global_query_currency_header+
                // "select SUM(mo) sumofamount from "+
                global_database_table_name+" where "+
                "country in (select country from "+ global_access_dataset_name +" where emailid = '"+session.message.address.user.id+"')"+
                // "And brand in (select brand from "+ global_access_dataset_name +" where emailid = '"+session.message.address.user.id+"') " +
                " OR fpi = ISNULL(fpi,"+(session.userData.global_kpi|| null)+") "+
                "OR scenario_code = ISNULL(scenario_code,'ACT') "+                            
                "OR brand = ISNULL(brand,"+(session.userData.global_brand|| null)+") "+
                "OR country = ISNULL(country,"+(session.userData.global_location|| null)+") "+
                " "+
                // "OR month = ISNULL(month,"+(global_month|| null)+") "+
                "OR date_updated2 BETWEEN  ISNULL(date_updated2,"+(from_date|| null)+") OR ISNULL(date_updated2,"+(to_date|| null)+") ", { type: sequelize.QueryTypes.SELECT})
                //"OR year = ISNULL(year,"+(global_year|| null)+") ", { type: sequelize.QueryTypes.SELECT})
                  .then(result => {
                    console.log(result);
                    console.log('\n\nSession month: '+session.userData.global_month);
                    console.log('\n\Final year: '+session.userData.global_year+'\n\n');
                    
                    if (session.userData.global_year_user_friendly == '') {
                        session.userData.global_year_user_friendly = '2018';
                    }
                    
                    
                    if ((session.userData.global_month_user_friendly != session.userData.global_last_month_in_db) && (session.userData.global_year_user_friendly != '2018')) {
                        console.log('Entered the block');
                        session.send('Sorry!! There is no data for this particular combination');            
                    }                                
                    // else if ((result[0].sumofamount) != null) {          // When the End Result is not null
                    else if (result.length != 0) { 
                        if ((result[0].sumofamount) == null) {
                            session.send(global_not_found_in_db);    
                        }                         
                        else if ((parseInt(session.userData.global_day) <= parseInt(final_day_in_database)) &&  parseInt(session.userData.global_day) > 0 && (session.userData.global_month == session.userData.global_last_month_in_db)) {                                 
                            session.say(`MTD ${session.userData.global_kpi_user_friendly} of ${session.userData.global_brand_user_friendly} for ${to_date_user_friendly}  in ${session.userData.global_location_user_friendly} is ${result[0].sumofamount}`,`MTD ${session.userData.global_kpi_user_friendly} of ${session.userData.global_brand_user_friendly} for ${to_date_user_friendly}  in ${session.userData.global_location_user_friendly} is ${result[0].sumofamount}`);  
                        }  
                        else {
                            session.send(global_not_found_in_db);
                        }                                                                    
                    }
                    else {
                        session.send(global_not_found_in_db);          
                    }
                  })
            }
            else {      // Monthly query

                sequelize.query(""+
                global_query_currency_header_monthly+
                // "select SUM(mo) sumofamount from "+
                global_database_table_name+" where "+
                "country in (select country from "+ global_access_dataset_name +" where emailid = '"+session.message.address.user.id+"')"+
                // "And brand in (select brand from "+ global_access_dataset_name +" where emailid = '"+session.message.address.user.id+"') " +
                " OR fpi = ISNULL(fpi,"+(session.userData.global_kpi|| null)+") "+
                "OR scenario_code = ISNULL(scenario_code,'ACT') "+                            
                "OR brand = ISNULL(brand,"+(session.userData.global_brand|| null)+") "+
                "OR country = ISNULL(country,"+(session.userData.global_location|| null)+") "+
                " "+
                // "OR month = ISNULL(month,"+(global_month|| null)+") "+
                "OR date_updated2 BETWEEN  ISNULL(date_updated2,"+(from_date|| null)+") OR ISNULL(date_updated2,"+(to_date|| null)+") ", { type: sequelize.QueryTypes.SELECT})
                //"OR year = ISNULL(year,"+(global_year|| null)+") ", { type: sequelize.QueryTypes.SELECT})
                  .then(result => {
                    console.log(result);
                    if (session.userData.global_month_user_friendly != final_month_in_database && session.userData.global_year != final_year_in_database) {
                        
                        session.send('Sorry!! There is no data for this particular combination');            
                    }                                
                    // else if ((result[0].sumofamount) != null) {          // When the End Result is not null
                    else if (result.length != 0) { 
                        if ((result[0].sumofamount) == null) {
                            session.send(global_not_found_in_db);    
                        }                         
                        else if ((parseInt(session.userData.global_day) <= parseInt(final_day_in_database)) &&  parseInt(session.userData.global_day) > 0) {                                 
                            session.say(`MTD ${session.userData.global_kpi_user_friendly} of ${session.userData.global_brand_user_friendly} for ${to_date_user_friendly}  in ${session.userData.global_location_user_friendly} is ${result[0].sumofamount}`,`MTD ${session.userData.global_kpi_user_friendly} of ${session.userData.global_brand_user_friendly} for ${to_date_user_friendly}  in ${session.userData.global_location_user_friendly} is ${result[0].sumofamount}`);  
                        }  
                        else {
                            session.send(global_not_found_in_db);
                        }                                                                    
                    }
                    else {
                        session.send(global_not_found_in_db);          
                    }
                  })
                                
            } // End of Else - Monthly query     
              
    } catch (error) {
            session.say(`I am sorry. I do not know the data`,`I am sorry. I do not know the data`);
    }
        
}	



function global_query_year_only(session, monthly_keyword_found) {
    console.log('\n\nEntered global query year only function\n\n');
    
    session.userData.global_monthly_keyword_found = monthly_keyword_found;
    
    try{
            /* Complete the sentence for empty brand and empty location */
        
            if (session.userData.global_brand_user_friendly == '') {
                session.userData.global_brand_user_friendly = 'RB';
            }
            
            if (session.userData.global_location_user_friendly == '') {
                session.userData.global_location_user_friendly = 'total';
            }
        
                                
            /*
                Retrieve the last month of the corresponding year
            */
            sequelize.query(""+
            "select CAST(DATENAME(month,DATEADD(mm,0,max(date_updated2))) AS CHAR(3)) as 'monthname' "+
            "from "+global_database_table_name+" "+
            "where year = " + session.userData.global_year + " "+
            " ", { type: sequelize.QueryTypes.SELECT})
              .then(result => {
                console.log(result);
                if ((result[0].monthname) != null) {          // When the End Result is not null                                  
                    session.userData.global_month = result[0].monthname;
                    session.userData.global_month_user_friendly = session.userData.global_month;
                    session.userData.global_month = "'"+session.userData.global_month+"'";                                                                        
                }
                else {
                     session.userData.global_month = '';
                     session.userData.global_month_user_friendly = '';      
                }
              })    
              
            var for_or_upto = '';
            
            /*** Get the last month from DB  ***/
            // getLastDayMonthYearFromDB().then(result_db_lastmonth => {  // Retrieve the Last Month from Database
            //     console.log("Matched value is: "+ result_db_lastmonth); // execution flow #8
            //     if (result_db_lastmonth != null) { 
            //         session.userData.global_last_month_in_db = result_db_lastmonth;    
            //     }
            //     else {
            //         session.userData.global_last_month_in_db = 'Mar';    
            //     }
            // })
            
            if (session.userData.global_month_user_friendly == session.userData.global_last_month_in_db)
            {
                for_or_upto = 'for';    
            }
            else {
                for_or_upto = 'upto';    
            }                      
        
            
            console.log('\n\nMonthly Keyword word found is:'+session.userData.global_monthly_keyword_found);
               
            if (session.userData.global_monthly_keyword_found == false) {  // Non-monthly query
                sequelize.query(""+
                "select case when (len(ABS(sum(summation))) - 10) > 9 then concat('£', cast(round(sum(summation)/1000000000,2) as numeric(36,2)),' Billion') "+
                "when (len(ABS(sum(summation))) - 10) > 6 and (len(ABS(sum(summation))) - 10) <= 9 then concat('£', cast(round(sum(summation)/1000000,2) as numeric(36,2)),' Million') "+
                "when (len(ABS(sum(summation))) - 10) > 3 and (len(ABS(sum(summation))) - 10) <= 6 then concat('£', cast(round(sum(summation)/1000,2) as numeric(36,2)),' K') "+
                "else FORMAT(sum(summation),'£#,0') end as sumofamount from (select year,month,fpi,brand,country,scenario_code,case when year = 2017 and month in "+
                "(select CAST(DATENAME(month,DATEADD(mm,0,max(date_updated2))) AS CHAR(3)) from "+global_database_table_name+" where year = 2017) then sum(ytd) when year = 2018 and month in (select CAST(DATENAME(month,DATEADD(mm,0,max(date_updated2))) AS CHAR(3)) from "+ global_database_table_name+ " "+
                "where year = 2018) then sum(ytd) when year = 2018 and month in (select CAST(DATENAME(month,DATEADD(mm,-1,max(date_updated2))) AS CHAR(3)) from "+global_database_table_name+" where year = 2018) "+
                "then sum(ytd) end as summation from "+global_database_table_name+" "+
                "where country in (select country from "+ global_access_dataset_name +" where emailid = '"+session.message.address.user.id+"')"+
                // "And brand in (select brand from "+ global_access_dataset_name +" where emailid = '"+session.message.address.user.id+"') " + 
                "group by year,month,fpi,brand,country,scenario_code)a where summation is not null and year = " + session.userData.global_year + " and fpi=" + session.userData.global_kpi + ""+
                "OR scenario_code = ISNULL(scenario_code,'ACT') "+                            
                "OR brand = ISNULL(brand,"+(session.userData.global_brand|| null)+") "+
                "OR country = ISNULL(country,"+(session.userData.global_location|| null)+") "+ 
                "group by year "+
                " ", { type: sequelize.QueryTypes.SELECT})
                  .then(result => {
                    console.log(result);
                    if (result.length != 0) {
                    // if ((result[0].sumofamount) != null) {          // When the End Result is not null
                        if ((result[0].sumofamount) != null) {                                   
                            session.say(`YTD ${session.userData.global_kpi_user_friendly} of ${session.userData.global_brand_user_friendly} ${for_or_upto} ${session.userData.global_month_user_friendly} ${session.userData.global_year_user_friendly} in ${session.userData.global_location_user_friendly} is ${result[0].sumofamount}`,`YTD ${session.userData.global_kpi_user_friendly} of ${session.userData.global_brand_user_friendly} ${for_or_upto} ${session.userData.global_month_user_friendly} ${session.userData.global_year_user_friendly} in ${session.userData.global_location_user_friendly} is ${result[0].sumofamount}`);
                        }
                        else {
                            session.send(global_not_found_in_db);    
                        }                                                                       
                    }
                    else {
                        session.send(global_not_found_in_db);   
                    }
                  })
            }
            else {                                                  // Monthly query       
                
                for_or_upto = 'for';                               // 'for' -> monthly MO
                                                                   
                sequelize.query(""+
                "select case when (len(ABS(sum(summation))) - 10) > 9 then concat('£', cast(round(sum(summation)/1000000000,2) as numeric(36,2)),' Billion') "+
                "when (len(ABS(sum(summation))) - 10) > 6 and (len(ABS(sum(summation))) - 10) <= 9 then concat('£', cast(round(sum(summation)/1000000,2) as numeric(36,2)),' Million') "+
                "when (len(ABS(sum(summation))) - 10) > 3 and (len(ABS(sum(summation))) - 10) <= 6 then concat('£', cast(round(sum(summation)/1000,2) as numeric(36,2)),' K') "+
                "else FORMAT(sum(summation),'£#,0') end as sumofamount from (select year,month,fpi,brand,country,scenario_code,case when year = 2017 and month in "+
                "(select CAST(DATENAME(month,DATEADD(mm,0,max(date_updated2))) AS CHAR(3)) from "+global_database_table_name+" where year = 2017) then sum(mo) when year = 2018 and month in (select CAST(DATENAME(month,DATEADD(mm,0,max(date_updated2))) AS CHAR(3)) from "+ global_database_table_name+ " "+
                "where year = 2018) then sum(mo) when year = 2018 and month in (select CAST(DATENAME(month,DATEADD(mm,-1,max(date_updated2))) AS CHAR(3)) from "+global_database_table_name+" where year = 2018) "+
                "then sum(mo) end as summation from "+global_database_table_name+" "+
                "where country in (select country from "+ global_access_dataset_name +" where emailid = '"+session.message.address.user.id+"')"+
                // "And brand in (select brand from "+ global_access_dataset_name +" where emailid = '"+session.message.address.user.id+"') " + 
                "group by year,month,fpi,brand,country,scenario_code)a where summation is not null and year = " + session.userData.global_year + " and fpi=" + session.userData.global_kpi + ""+
                "OR scenario_code = ISNULL(scenario_code,'ACT') "+                            
                "OR brand = ISNULL(brand,"+(session.userData.global_brand|| null)+") "+
                "OR country = ISNULL(country,"+(session.userData.global_location|| null)+") "+ 
                "group by year "+
                " ", { type: sequelize.QueryTypes.SELECT})
                  .then(result => {
                    console.log(result);
                    if (result.length != 0) {
                    // if ((result[0].sumofamount) != null) {          // When the End Result is not null
                        if ((result[0].sumofamount) != null) {                                   
                            session.say(`MO ${session.userData.global_kpi_user_friendly} of ${session.userData.global_brand_user_friendly} ${for_or_upto} ${session.userData.global_month_user_friendly} ${session.userData.global_year_user_friendly} in ${session.userData.global_location_user_friendly} is ${result[0].sumofamount}`,`MO ${session.userData.global_kpi_user_friendly} of ${session.userData.global_brand_user_friendly} ${for_or_upto} ${session.userData.global_month_user_friendly} ${session.userData.global_year_user_friendly} in ${session.userData.global_location_user_friendly} is ${result[0].sumofamount}`);
                        }
                        else {
                            session.send(global_not_found_in_db);    
                        }                                                                       
                    }
                    else {
                        session.send(global_not_found_in_db);   
                    }
                  })                                            
            }         // End of else - Monthly data           
              
    }  // End of Try block
    catch (error) {
            session.say(`I am sorry. I do not know the data`,`I am sorry. I do not know the data`);
    }      
}





function boilerPlateCodeIntents(session, intent) {
    
    session.userData.global_monthly_keyword_found = false;  // place 'monthly' in context  - true or false
    // getLastMonthInDB(); 
    
    
    
    getLastMonthInDB().then(result_db_lastmonth => {  // Retrieve the Last Date from Database
                            console.log("Matched value is: "+ result_db_lastmonth); // execution flow #8
                            if (result_db_lastmonth != null) {
                                session.userData.global_last_month_in_db = result_db_lastmonth;    
                            }
                            else {
                                session.userData.global_last_month_in_db = '';      
                            }
    });  

    
    getLastDayInDB().then(result_db_lastday => {  // Retrieve the Last Date from Database
                        console.log("Matched value is: "+ result_db_lastday); // execution flow #8
                        if (result_db_lastday != null) {
                            session.userData.global_last_day_in_db = result_db_lastday;    
                        }
                        else {
                            session.userData.global_last_day_in_db = '';      
                        }
    });   
    
        
    var brand = builder.EntityRecognizer.findEntity(intent.entities, 'Brand');
    var datetimeV2 = builder.EntityRecognizer.findEntity(intent.entities, 'builtin.datetimeV2.date');
    
    var monthly = builder.EntityRecognizer.findEntity(intent.entities, 'Monthly');
      
    if (monthly) {                                             // Monthly entity keyword is found by LUIS
        if (monthly.resolution.values[0] != "") {             // If Monthly keyword values array in LUIS JSON is not empty
            session.userData.global_monthly_keyword_found = true;             // Get the 1st element from LUIS JSON values array
        }
        else {
            session.userData.global_monthly_keyword_found = false; 
        }                
    } 
    
        
    if (brand) {               
        console.log('LUIS Brand name is: '+brand.resolution.values[0]);
        
        if (brand.resolution.values[0] == "RB") {
            session.userData.global_brand_filter_string = brand.resolution.values[0];
            session.userData.global_brand = "";   
            session.userData.global_brand_user_friendly = session.userData.global_brand; 
        }
        else if (brand.resolution.values[0] != "") {             // If Brand values array in LUIS JSON is not empty
            session.userData.global_brand = "" + brand.resolution.values[0]; // Get the 1st element from LUIS JSON values array, and convert to String
            // global_brand = brand.entity;
            /* Sql parameter */
            session.userData.global_brand_user_friendly = session.userData.global_brand;
            session.userData.global_brand = "'"+session.userData.global_brand+"'";
            session.userData.global_brand_filter_string = "";
        }        
        else {
            session.userData.global_brand = brand.entity;
            session.userData.global_brand_user_friendly = session.userData.global_brand;
            session.userData.global_brand = "'"+session.userData.global_brand+"'";
            session.userData.global_brand_filter_string = "";
        }
        
    } 
    
    
    
    
    var location = builder.EntityRecognizer.findEntity(intent.entities, 'Location');
    
    if (location) {
        // global_year = year.entity;
        
        if (location.resolution.values[0] != "") {             // If Year values array in LUIS JSON is not empty
            session.userData.global_location = "" + location.resolution.values[0]; // Get the 1st element from LUIS JSON values array, and convert to String
        }
        else {
            session.userData.global_location = location.entity;
        }
        
        
        session.userData.global_location_user_friendly = session.userData.global_location;
        /* Sql parameter */
        session.userData.global_location = "'"+session.userData.global_location+"'";      
        
    
        // global_month = nodeUcfirst(global_month);            
        //session.send('Identified location entity is %s.', location.entity);
    }   
    
    
    
    
         
    var month = builder.EntityRecognizer.findEntity(intent.entities, 'Month');
    
    if (month) {
        if (month.resolution.values[0] != "") {             // If Month values array in LUIS JSON is not empty
            session.userData.global_month = "" + month.resolution.values[0]; // Get the 1st element from LUIS JSON values array, and convert to String
        }
        else {
            session.userData.global_month = month.entity;
        }
        session.userData.global_month = nodeUcfirst(session.userData.global_month); 
        session.userData.global_month_user_friendly = session.userData.global_month;
        /* Sql parameter */
        session.userData.global_month = "'"+session.userData.global_month+"'";                          
        //session.send('Identified location entity is %s.', location.entity);
    }
       
    var year = builder.EntityRecognizer.findEntity(intent.entities, 'Year');
    
    if (year) {
        // global_year = year.entity;
        
        if (year.resolution.values[0] != "") {             // If Year values array in LUIS JSON is not empty
            session.userData.global_year = "" + year.resolution.values[0]; // Get the 1st element from LUIS JSON values array, and convert to String
        }
        else {
            session.userData.global_year = year.entity;
        }
        
        
        session.userData.global_year_user_friendly = session.userData.global_year;
        /* Sql parameter */
        session.userData.global_year = "'"+session.userData.global_year+"'";      
        
        
        session.userData.value_passing = 'Hello, how are you?';
                
        // global_month = nodeUcfirst(global_month);            
        //session.send('Identified location entity is %s.', location.entity);
    }   
    
    
    var current_month = builder.EntityRecognizer.findEntity(intent.entities, 'Current Month');    
    var previous_month = builder.EntityRecognizer.findEntity(intent.entities, 'Previous Month');  
    
    if ((current_month) || (previous_month)) {   // When Month is not identified by LUIS
        
        console.log('Current month or previous month');
    
        if (current_month) {
            session.userData.global_month = getCurrentMonth();            
            // session.userData.global_month = 'Apr';
            
            session.userData.global_month = nodeUcfirst(session.userData.global_month); 
            session.userData.global_month_user_friendly = session.userData.global_month;
            /* Sql parameter */
            session.userData.global_month = "'"+session.userData.global_month+"'";
            
            
            session.userData.global_year = getCurrentYear();
            // session.userData.global_year = '2018';
            
            session.userData.global_year_user_friendly = session.userData.global_year;
            /* Sql parameter */
            session.userData.global_year = "'"+session.userData.global_year+"'";  
        }
        else {
            session.userData.global_month = getPreviousMonth();
            // session.userData.global_month = 'Mar';
            
            session.userData.global_month = nodeUcfirst(session.userData.global_month); 
            session.userData.global_month_user_friendly = session.userData.global_month;
            /* Sql parameter */
            session.userData.global_month = "'"+session.userData.global_month+"'";
            
            
            session.userData.global_year = getCurrentYear();
            // session.userData.global_year = '2018';
            
            session.userData.global_year_user_friendly = session.userData.global_year;
            /* Sql parameter */
            session.userData.global_year = "'"+session.userData.global_year+"'";                    
        }             
    }
    

        if ((month) && !(year)) {
        
            session.userData.global_year = getCurrentYear();
            session.userData.global_year_user_friendly = session.userData.global_year;
            session.userData.global_year = "'"+session.userData.global_year+"'";
        
            // if ((global_month_user_friendly == getCurrentMonth()) && (global_year_user_friendly == getCurrentYear())) {
            //     global_query(session, 'MTD', 'for');   
            // }
            // else {
            //     global_query(session, 'YTD', 'upto');       
            // } 
        }
         
         
        console.log('Month identified is: ' + session.userData.global_month); 
        
        console.log('\n\nThe global brand filter string is: '+!brandFilterExists(session.userData.global_brand_filter_string));
        
        if ((brand) && !(month) && !(year) && !(location) && (brandFilterExists(session.userData.global_brand_filter_string) == false)) {
            session.beginDialog('AskLocationNestedDialog'); 
            // session.send('Ask location Nested dialog is handled');
        }        
        
        else if ((month) && (year) && (brand) && !(location) && (brandFilterExists(session.userData.global_brand_filter_string) == false)) {
            session.beginDialog('AskLocationDialog');   
            // session.send('Ask location dialog is handled');
        }
        
        else if (!(month) && !(current_month) && !(previous_month) && (year)){
            session.userData.global_month = '';
            session.userData.global_month_user_friendly = '';
            global_query_year_only(session, session.userData.global_monthly_keyword_found);
            // session.send('Global query year only');
        } 
        
        else if (!(month) && ((current_month) || (previous_month)) && (year)) {
            if ((current_month) && (session.userData.global_month_user_friendly == session.userData.global_last_month_in_db)) {
                global_query(session, 'MTD', 'for', session.userData.global_monthly_keyword_found);
                // session.send('Global query');
            }
            else if ((previous_month) && (session.userData.global_month_user_friendly == session.userData.global_last_month_in_db)) {
                global_query(session, 'MTD', 'for', session.userData.global_monthly_keyword_found);
                // session.send('Global query');
            }
            else { 
                global_query(session, 'YTD', 'upto', session.userData.global_monthly_keyword_found);
                // session.send('Global query');
            }
        } 
        
        else if (!(month) && ((current_month) || (previous_month)) && !(year)) {
            if ((current_month) && (session.userData.global_month_user_friendly == session.userData.global_last_month_in_db)) {
                global_query(session, 'MTD', 'for', session.userData.global_monthly_keyword_found);
                // session.send('Global query');
            }
            else if ((previous_month) && (session.userData.global_month_user_friendly == session.userData.global_last_month_in_db)) {
                global_query(session, 'MTD', 'for', session.userData.global_monthly_keyword_found);
                // session.send('Global query');
            }
            else { 
                global_query(session, 'YTD', 'upto', session.userData.global_monthly_keyword_found);
                // session.send('Global query');
            }
        }  
         
        else if(!(month) && !(year) && !(current_month) && !(previous_month)) {
            session.beginDialog('AskMonthYearDialog');
            // session.send('AskMonthYearDialog');
        }              
        
        else {    
            
            // str_query = ""+
            //       "select SUM(mo) sumofamount from Rbfindata where "+
            // "fpi = ISNULL(fpi,"+(global_kpi|| null)+") "+
            // "OR brand = ISNULL(brand,"+(global_brand|| null)+") "+
            // "OR country = ISNULL(country,"+(global_location|| null)+") "+
            // "OR month = ISNULL(month,"+(global_month|| null)+") "+
            // "OR year = ISNULL(year,"+(global_year|| null)+") ";
                      
            // console.log(str_query);
            
             
             if ((session.userData.global_month_user_friendly == session.userData.global_last_month_in_db) && (session.userData.global_year_user_friendly == '2018')) {
                global_query(session, 'MTD', 'for', session.userData.global_monthly_keyword_found);   
                // session.send('Global query');
             }
             else {
                global_query(session, 'YTD', 'upto', session.userData.global_monthly_keyword_found);  
                // session.send('Global query');     
             } 
                     
        }    // Else Closing Block - When Month and Year are Specified, go to DB
    
}


function boilerPlateCodeMonthYear(session, user_response, dialog_name){
        // query_string = "https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/"+luisAppId+"?subscription-key="+luisAPIKey+"&verbose=false&q="+user_response;
        // getLastMonthInDB(); // Get the last month from database
        
        var user_response_lowercase = user_response.toLowerCase();
        
        console.log('\n\nUser response is:  '+user_response_lowercase);
        
        var prefix_string = "Gross Margin for ";
        
        user_response = prefix_string + user_response;        
        
        user_response = user_response.replace(" ","%20");  // Replace whitespace with its ASCII character for Luis REST API
        
        query_string = global_luis_bing_rest_api + user_response;
        
        console.log(query_string);
          
        // getLastDayInDB();  
        
        getLastMonthInDB().then(result_db_lastmonth => {  // Retrieve the Last Date from Database
                                console.log("\n\nMatched last month value is: "+ result_db_lastmonth); // execution flow #8
                                if (result_db_lastmonth != null) {
                                    session.userData.global_last_month_in_db = result_db_lastmonth;    
                                }
                                else {
                                    session.userData.global_last_month_in_db = '';      
                                }
        });
        
        getLastDayInDB().then(result_db_lastday => {  // Retrieve the Last Date from Database
                                console.log("Matched value is: "+ result_db_lastday); // execution flow #8
                                if (result_db_lastday != null) {
                                    session.userData.global_last_day_in_db = result_db_lastday;    
                                }
                                else {
                                    session.userData.global_last_day_in_db = '';      
                                }
        });          
        
        
        
        console.log('Global last month in db is: '+session.userData.global_last_month_in_db)
        // var from_date = '1/' + session.userData.global_last_month_in_db + '/'+ getCurrentYear();  
        var from_date = '1/Apr/2018';
                        
        request(query_string, function (error, response, body) {
                console.log('error:', error); 
                console.log('statusCode:', response && response.statusCode);
                // console.log(body);
                var result_json = JSON.parse(body);
                
                console.log(result_json);
                
                var day = builder.EntityRecognizer.findEntity(result_json.entities, 'Day'); 
                if (day != null)
                    session.userData.global_day = day.entity;
                
                else
                    session.userData.global_day = '';
                
                console.log(day);
                
                console.log('Inside the request:');
                console.log(session.userData.global_day+" "+" global day");
                
                month = builder.EntityRecognizer.findEntity(result_json.entities, 'Month');
                console.log(month);
                
                if (month) {            
                    if (month.resolution.values[0] != "") {             // If Month values array in LUIS JSON is not empty
                        session.userData.global_month = "" + month.resolution.values[0]; // Get the 1st element from LUIS JSON values array, and convert to String
                        session.userData.global_month_user_friendly = session.userData.global_month;
                    }
                    else {
                        session.userData.global_month = month.entity;
                        session.userData.global_month_user_friendly = session.userData.global_month;
                    }
                }
                
                year = builder.EntityRecognizer.findEntity(result_json.entities, 'Year');
                console.log(year);
                
                if (year) {            
                    if (year.resolution.values[0] != "") {             // If Year values array in LUIS JSON is not empty
                        session.userData.global_year = "" + year.resolution.values[0]; // Get the 1st element from LUIS JSON values array, and convert to String
                        session.userData.global_year_user_friendly = session.userData.global_year;
                    }
                    else {
                        session.userData.global_year = year.entity;
                        session.userData.global_year_user_friendly = session.userData.global_year;
                    }
                }
                else {
                        session.userData.global_year = '2018';
                        session.userData.global_year_user_friendly = session.userData.global_year;
                }                
                
                

                if (checkExists(user_response_lowercase)) {
                            //yesterday_today(session);
                            // var to_date = '17/Mar/2018';       // Last date of monthly data
                            
                            // Store Last day in DB into a global variable                               
                            var to_date = session.userData.global_last_day_in_db +'/'+ session.userData.global_last_month_in_db +'/'+getCurrentYear();       // Last date of monthly data//
                            // var to_date = '17/Mar/2018';
                            console.log('\nTo Date is: '+to_date+' \n');
                
                            global_query_daily(session, from_date, to_date, session.userData.global_last_day_in_db, session.userData.global_last_month_in_db, session.userData.global_monthly_keyword_found);                
                            // global_query_daily(session, from_date, to_date, '17', 'Mar', session.userData.global_monthly_keyword_found);                            
                            // session.send('Global query daily');
                }
                        
                else if (session.userData.global_day != "" && (checkExists(user_response_lowercase) == false) && (month)) {            
                    
                    console.log('Inside Else if:');
                    console.log(session.userData.global_day+" "+" global day");  
             
                    var year_for_daily_query = '2018';  // Get the current year for daily query
                    

                    var three_letter_month = getThreeLetterMonthNames(session.userData.global_month);
                    
                    console.log('global_month variable'); 
                    console.log(three_letter_month);
                    
                    var to_date = ""+session.userData.global_day+"/"+three_letter_month+"/"+year_for_daily_query;
                    
                    // Store Last day in DB into a global variable
                    // getLastDayInDB();                     
                    global_query_daily(session, from_date, to_date, session.userData.global_last_day_in_db, session.userData.global_last_month_in_db, session.userData.global_monthly_keyword_found);
                    // global_query_daily(session, from_date, to_date, '17', 'Mar', session.userData.global_monthly_keyword_found);
                    // session.send('Global query daily');            
                }  // Closing of else if
        
                else {                                                  // Else starts
                                
                    query_string = global_luis_bing_rest_api + user_response;
                    
                    request(query_string, function (error, response, body) {
                            console.log('error:', error); 
                            console.log('statusCode:', response && response.statusCode);
                            // console.log(body);
                            var result_json = JSON.parse(body);
                            console.log(result_json.entities);
                            
                            var month = builder.EntityRecognizer.findEntity(result_json.entities, 'Month');
                            var year = builder.EntityRecognizer.findEntity(result_json.entities, 'Year');
  
                            /***** (START) - Check for current month or previous month entities from LUIS *****/
                            var current_month = builder.EntityRecognizer.findEntity(result_json.entities, 'Current Month');    
                            var previous_month = builder.EntityRecognizer.findEntity(result_json.entities, 'Previous Month');  
                            
                            if ((current_month) || (previous_month)) {   // When Month is not identified by LUIS
                                
                                console.log('Current month or previous month');
                            
                                if (current_month) {
                                    
                                    session.userData.global_month = getCurrentMonth();
                                    // session.userData.global_month = 'Apr';
                                    
                                    session.userData.global_month = nodeUcfirst(session.userData.global_month); 
                                    session.userData.global_month_user_friendly = session.userData.global_month;
                                    /* Sql parameter */
                                    session.userData.global_month = "'"+session.userData.global_month+"'";
                                    
                                    
                                    session.userData.global_year = '2018';
                                    
                                    session.userData.global_year_user_friendly = session.userData.global_year;
                                    /* Sql parameter */
                                    session.userData.global_year = "'"+session.userData.global_year+"'";  
                                }
                                else {
                                    session.userData.global_month = getPreviousMonth();
                                    // session.userData.global_month = 'Mar';
                                    
                                    session.userData.global_month = nodeUcfirst(session.userData.global_month); 
                                    session.userData.global_month_user_friendly = session.userData.global_month;
                                    /* Sql parameter */
                                    session.userData.global_month = "'"+session.userData.global_month+"'";
                                    
                                    
                                    // session.userData.global_year = getCurrentYear();
                                    session.userData.global_year = '2018';
                                    
                                    session.userData.global_year_user_friendly = session.userData.global_year;
                                    /* Sql parameter */
                                    session.userData.global_year = "'"+session.userData.global_year+"'";                    
                                }             
                            }  
                            /***** (END) - Check for current month or previous month entities from LUIS *****/
                            
                            if(!(month) && !(year) && !(current_month) && !(previous_month)) {
                                // location_not_specified(session);                        
                                session.send(global_month_not_found);
                                
                                // Re-ask the same dialog       
                                session.beginDialog(dialog_name);                                    
                            }
                            
                            else if (!(month) && ((current_month) || (previous_month)) && (year)) {
                                
                                if ((current_month) && (session.userData.global_month_user_friendly == session.userData.global_last_month_in_db)) {
                                    global_query(session, 'MTD', 'for', session.userData.global_monthly_keyword_found);
                                    // session.send('Global query'); 
                                }
                                else if ((previous_month) && (session.userData.global_month_user_friendly == session.userData.global_last_month_in_db)) {
                                    global_query(session, 'MTD', 'for', session.userData.global_monthly_keyword_found);
                                    // session.send('Global query'); 
                                }
                                else { 
                                    global_query(session, 'YTD', 'upto', session.userData.global_monthly_keyword_found);
                                    // session.send('Global query'); 
                                }
                            }

                            else if (!(month) && ((current_month) || (previous_month)) && !(year)) {
                                
                                if ((current_month) && (session.userData.global_month_user_friendly == session.userData.global_last_month_in_db)) {
                                    global_query(session, 'MTD', 'for', session.userData.global_monthly_keyword_found);
                                    // session.send('Global query'); 
                                }
                                else if ((previous_month) && (session.userData.global_month_user_friendly == session.userData.global_last_month_in_db)) {
                                    global_query(session, 'MTD', 'for', session.userData.global_monthly_keyword_found);
                                    // session.send('Global query'); 
                                }
                                else { 
                                    global_query(session, 'YTD', 'upto', session.userData.global_monthly_keyword_found);
                                    // session.send('Global query'); 
                                }
                            }                            
                            
                            else if (!(month) && (year)) {
                                if (year.resolution.values[0] != "") {             // If Year values array in LUIS JSON is not empty
                                    session.userData.global_year = "" + year.resolution.values[0]; // Get the 1st element from LUIS JSON values array, and convert to String
                                }
                                else {
                                    session.userData.global_year = year.entity;
                                }
                                session.userData.global_year_user_friendly = session.userData.global_year;
                                session.userData.global_year = "'"+session.userData.global_year+"'";  
                                global_query_year_only(session, session.userData.global_monthly_keyword_found);  
                                // session.send('Global query year only');  
                            }
                            
                            else if ((month) && !(year)) {
                                session.userData.global_month_user_friendly = session.userData.global_month;
                                session.userData.global_month = "'"+session.userData.global_month+"'";
                                
                                // session.userData.global_year = getCurrentYear();
                                session.userData.global_year = '2018';
                                
                                session.userData.global_year_user_friendly = session.userData.global_year;
                                session.userData.global_year = "'"+session.userData.global_year+"'";
                                
                                if ((session.userData.global_month_user_friendly == session.userData.global_last_month_in_db) && (session.userData.global_year_user_friendly == '2018')) {
                                    global_query(session, 'MTD', 'for', session.userData.global_monthly_keyword_found);
                                    // session.send('Global query');    
                                }
                                else {
                                    global_query(session, 'YTD', 'upto', session.userData.global_monthly_keyword_found); 
                                    // session.send('Global query');       
                                } 
                            }
                                                        
                            else if ((month) && (year)) {
                                /* Sql variables */                    
                                if (month.resolution.values[0] != "") {             // If Month values array in LUIS JSON is not empty
                                    session.userData.global_month = "" + month.resolution.values[0]; // Get the 1st element from LUIS JSON values array, and convert to String
                                }
                                else {
                                    session.userData.global_month = month.entity;
                                }
                                
                              
                                session.userData.global_month = nodeUcfirst(session.userData.global_month); // First character - uppercase
                                
                                session.userData.global_month_user_friendly = session.userData.global_month;
                                session.userData.global_month = "'"+session.userData.global_month+"'";  
                                
                                // global_year = year.entity;
                                
                                if (year.resolution.values[0] != "") {             // If Year values array in LUIS JSON is not empty
                                    session.userData.global_year = "" + year.resolution.values[0]; // Get the 1st element from LUIS JSON values array, and convert to String
                                }
                                else {
                                    session.userData.global_year = year.entity;
                                }
                                
                                
                                session.userData.global_year_user_friendly = session.userData.global_year;  
                                session.userData.global_year = "'"+session.userData.global_year+"'";
                            
         
                                if ((session.userData.global_month_user_friendly == session.userData.global_last_month_in_db) && (session.userData.global_year_user_friendly == '2018')) {
                                    global_query(session, 'MTD', 'for', session.userData.global_monthly_keyword_found); 
                                    // session.send('Global query');   
                                }
                                else {
                                    global_query(session, 'YTD', 'upto', session.userData.global_monthly_keyword_found);
                                    // session.send('Global query');        
                                } 
         
         				} // Closing of if -  month and year
                         
                    });  // Closing of the REST API - send receive block
                         
                 }   // Else ends                
            
        });
                      
        session.endDialog();           
}

function commonCodeIntents(session, intent, kpi, kpi_user_friendly) {
    clearGlobalVariablesIntents(session);
                                
    session.userData.global_kpi = kpi;
    /* Sql parameter */
    session.userData.global_kpi = "'"+session.userData.global_kpi+"'"; 
    session.userData.global_kpi_user_friendly = kpi_user_friendly;
    
    console.log(intent.entities);
    
    /*** STORE INTO CHAT HISTORY  ***/
    email_id = session.message.address.user.id;
    //store_chat_history(email_id, session.userData.global_kpi, session.message.text);        
}

function clearGlobalVariablesIntents(session) {
    session.userData.global_brand = '';
    session.userData.global_brand_user_friendly = '';
    session.userData.global_brand_filter_string = '';
    
    session.userData.global_location = '';
    session.userData.global_location_user_friendly = ''
    
    session.userData.global_day = '';
    session.userData.global_day_user_friendly = '';
    
    session.userData.global_month = '';
    session.userData.global_month_user_friendly = '';
    
    session.userData.global_year = '';
    session.userData.global_year_user_friendly = '';
        
    session.userData.global_kpi = '';
    session.userData.global_kpi_user_friendly = ''; 
    
    session.userData.merged_month_year = '';
    
    session.userData.global_monthly_keyword_found = false;       
            
}

